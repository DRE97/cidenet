CIDENET USERS
. Realizada por: Daniel Ramirez Echeverri

Tecnologias utilizadas:
 React, Nodejs, express, mongodb

Guia de configuracion:
1. Clonar o descargar el archivo ZIP desde GiTHub y abrir en editor
2. Abrir una nueva terminal en la ubicacion del archivo
3. Ejecutar el comando 'cd cidenet_server'
4. Ejecutar el comando npm install
5. Ejecutar el comando npm start
6. Abrir una nueva terminal
7. Ejecutar el comando 'cd cidenet_front'
8. Ejecutar el comando npm install
9. Ejecutar el comando npm start
