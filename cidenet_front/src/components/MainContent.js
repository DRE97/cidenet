import React, { useState, useEffect } from 'react';

// Styled components
import SearchList from './partialComponents/SearchList';
import UsersList from './UsersList';
import NewUserForm from './NewUserForm';

// Helpers
import UserHelpers from '../helpers/users.helper'

export default function MainContent({ content }) {

    const [users, setUsers] = useState([]);
    const [filteredUsers, setFilteredUsers] = useState([]);

    const getUsers = async () => {
        const allUsers = await UserHelpers.getAllUsers();
        setUsers(allUsers);
        setFilteredUsers(allUsers);
    }

    useEffect(() => {
        getUsers();
    }, [])
    
    const filterUsers = (category, value) => {
        console.log(value);
        if(value.length > 1) {
            const newUsers = users.filter((user) => 
                user[category].includes(value)
            );
            setFilteredUsers(newUsers);
        } else {
            setFilteredUsers(users)
        }
    }

    return (
        <div className="main-content w-full mx-2 lg:w-5/6 h-full">
          <SearchList filterUsers={filterUsers} />
          <div className="w-full lg:w-11/12 h-4/5 my-4 bg-white rounded-xl mx-auto overflow-y-scroll">
                {
                    content === 'users' ?
                    <h1 className="mt-6 ml-6 text-3xl 2xl:text-5xl">Usuarios</h1> :
                    <h1 className="mt-6 ml-6 text-3xl 2xl:text-5xl">Nuevo Usuario</h1>
                }
                {
                    content === 'users' ?
                    <UsersList users={filteredUsers} /> :
                    <NewUserForm />
                }
          </div>
        </div>
    )
}
