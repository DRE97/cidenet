import React, { useState } from 'react'
import cidenet_logo from '../images/logo.PNG';
import { FaUsers } from 'react-icons/fa';
import { TiUserAdd } from 'react-icons/ti';

export default function Navbar({ setContent, content }) {
    const [isHidden, setIsHidden] = useState(true);

    const onClick = (e) => {
        if(document.querySelector('li.active')) {
            const prevClass = document.querySelector('li.active').className
            document.querySelector('li.active').className = prevClass.replace('active', '');
        }
        e.target.className += ' active'
        setContent(e.target.id);
    }

    const toggleElements = () => {
        let classes = document.querySelector('.nav-items').className
        if(!isHidden) {
            document.querySelector('.nav-items').className = classes.replace('absolute', 'hidden')
            setIsHidden(true)
        } else {
            document.querySelector('.nav-items').className = classes.replace('hidden', 'absolute')
            setIsHidden(false)
        }
    }

    return (
        <div className="navbar w-full lg:w-1/6 h-20 lg:h-full flex justify-between lg:block bg-white rounded-lg">
            <a href="/">
                <img src={ cidenet_logo } alt="Logo" className="h-16 lg:h-auto lg:w-2/3 lg:mx-auto" />
            </a>
            <button onClick={() => toggleElements()} className="inline-block lg:hidden w-12 h-12 text-gray-600 p-1 my-auto mr-5">
                <svg fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
            </button>
            <ul className="nav-items hidden lg:flex flex-col justify-center my-auto mx-auto lg:my-0 lg:mt-20 px-6 py-5 top-20 right-0 bg-white lg:bg-transparent rounded-2xl">
                <li
                    className={`text-center py-3 px-8 flex flex-row justify-between 2xl:justify-evenly ${content === 'users' ? 'active' : ''}`}
                    id="users"
                    onClick={(e) => onClick(e)}
                >
                    <FaUsers className="my-auto" />
                    Ver Usuarios
                </li>
                <li
                    className={`text-center py-3 px-8 flex flex-row justify-between 2xl:justify-evenly ${content === 'new-user' ? 'active' : ''}`}
                    id="new-user"
                    onClick={(e) => onClick(e)}
                >
                    <TiUserAdd className="my-auto" />
                    Nuevo usuario
                </li>
            </ul>
            <div className="hidden lg:flex lg:flex-col text-center text-gray-400 lg:absolute lg:bottom-10 lg:left-16 2xl:left-24">
                <h1>Creado por:</h1>
                <h4>Daniel Ramirez</h4>
            </div>
        </div>
    )
}