import React, { useState } from 'react'

// Partial components
import User from './partialComponents/User'
import Pagination from './partialComponents/Pagination'
import EditModal from './partialComponents/EditModal'

export default function UsersList({ users }) {

    const [openModal, setOpenModal] = useState(false)
    const [userToEdit, setUserToEdit] = useState({});
    const [currentPage, setCurrentPage] = useState(1);
    const [usersPerPage] = useState(10);

    const columns = ['Nombre', 'Pais', 'Identificacion', 'Email', 'Fecha Ingreso', 'Area', 'Estado', 'Fecha de registro', 'Opciones'];

    const indexOfLastUser = currentPage * usersPerPage;
    const indexOfFirstUser = indexOfLastUser - usersPerPage;
    const currentUsers = users.slice(indexOfFirstUser, indexOfLastUser);

    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    return (
        <div className="flex flex-col justify-start p-4 2xl:px-0">
            <table className="w-full text-center">
                <thead>
                    <tr className="h-20 px-10">
                        {
                            columns.map((col, index) => (
                                <td key={index} className="px-10">{col}</td>
                            ))
                        }
                    </tr>
                </thead>
                <tbody>
                    {
                        currentUsers.map((user, index) => (
                            <User key={index} user={user} setOpenModal={setOpenModal} setUserToEdit={setUserToEdit} />
                        ))
                    }
                </tbody>
            </table>

            <Pagination usersPerPage={usersPerPage} totalUsers={users.length} paginate={paginate} />

            {
                userToEdit._id ?
                <EditModal openModal={openModal} setOpenModal={setOpenModal} userToEdit={userToEdit} setUserToEdit={setUserToEdit} /> :
                <></>
            }
        </div>
    )
}