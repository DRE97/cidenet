import React, { useState } from 'react'           

export default function SearchList({ filterUsers }) {
    const [searchCategory, setSearchCategory] = useState('primer_nombre')
    let searchValue = '';

    const handleChange = (e) => {
        const tag = e.target;
        if(tag.tagName === 'SELECT') {
            setSearchCategory(tag.value);
        } else if (tag.tagName === 'INPUT') {
            searchValue = tag.value
        }
        filterUsers(searchCategory, searchValue);
    }

    return (
        <div className="w-full lg:w-11/12 rounded-xl mx-auto mt-6 lg:my-10 bg-transparent">
            <form className="flex flex-col lg:flex-row lg:justify-start ">
                <select
                    className="form-select mx-2 w-11/12 lg:w-1/6 rounded-xl"
                    aria-label="search item"
                    onChange={(e) => handleChange(e)}
                >
                    <option value="primer_nombre">Primer Nombre</option>
                    <option value="segundo_nombre">Segundo Nombre</option>
                    <option value="primer_apellido">Primer Apellido</option>
                    <option value="segundo_apellido">Segundo Apellido</option>
                    <option value="tipo_identificacion">Tipo de Identificación</option>
                    <option value="numero_identificacion">Número de Identificación</option>
                    <option value="pais">País</option>
                    <option value="email">Email</option>
                </select>

                <input
                    className="form-control mx-2 w-11/12 mt-2.5 lg:mt-0 lg:w-1/3 rounded-xl"
                    type="search"
                    placeholder="Buscar por ..."
                    aria-label="Search"
                    onChange={(e) => handleChange(e)}
                />
                
            </form>
        </div>
    )
}

/*



*/