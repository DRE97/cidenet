import React from 'react'

export default function Pagination({ usersPerPage, totalUsers, paginate }) {
    const pageNumbers = [];

    for(let i=1; i <= Math.ceil(totalUsers / usersPerPage); i++) {
        pageNumbers.push(i)
    }

    const changePage = (e, number) => {
        e.preventDefault();
        paginate(number)
    }

    return (
        <nav className="mt-6 ">
            <ul className="pagination">
                {
                    pageNumbers.map(number => (
                        <li key={number} className="page-item">
                            <a onClick={(e) => changePage(e, number)} href="!#" className="page-link">
                                {number}
                            </a>
                        </li>
                    ))
                }
            </ul>
        </nav>
    )
}
