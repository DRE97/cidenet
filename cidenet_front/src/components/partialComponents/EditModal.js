import React from 'react'
import { ModalContent, StaticDialog } from 'react-st-modal';
import UserToEdit from './UserToEdit'

export default function EditModal({ openModal, setOpenModal, userToEdit, setUserToEdit }) {
    console.log(userToEdit);
    return (
        <StaticDialog
            className="edit-modal"
            isOpen={openModal}
            isCanClose={false}
            showCloseIcon={true}
            onAfterClose={() => setOpenModal(false)}
            title="Editar usuario"
        >
            <ModalContent>
                <UserToEdit userToEdit={userToEdit} setOpenModal={setOpenModal} setUserToEdit={setUserToEdit} />
            </ModalContent>
        </StaticDialog>
    );
}
