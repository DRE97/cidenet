import React, { useState, useEffect } from 'react';
import usersHelper from '../../helpers/users.helper';

// Helpers
import UserHelpers from '../../helpers/users.helper';

export default function UserToEdit({ userToEdit, setOpenModal, setUserToEdit }) {
    const user = userToEdit;
    const [primerNombre, setPrimerNombre] = useState(user.primer_nombre);
    const [nameChange, setNameChange] = useState(false)
    const [segundoNombre, setSegundoNombre] = useState(user.segundo_nombre);
    const [primerApellido, setPrimerApellido] = useState(user.primer_apellido);
    const [lastNameChange, setLastNameChange] = useState(false)
    const [segundoApellido, setSegundoApellido] = useState(user.segundo_apellido);
    const [pais, setPais] = useState(user.pais);
    const [tipoIdentificacion, setTipoIdentificacion] = useState(user.tipo_identificacion);
    const [numeroIdentificacion, setNumeroIdentificacion] = useState(user.numero_identificacion);
    const [documentChange, setDocumentChange] = useState(false)
    const [ingreso, setIngreso] = useState('');
    const [area, setArea] = useState(user.area);

    const [maxDate, setMaxDate] = useState('');
    const [minDate, setMinDate] = useState('');

    useEffect(() => {
        const date = UserHelpers.getDateConstrains();
        setMaxDate(date.max);
        setMinDate(date.min);
    }, [])

    const handleNameChange = (e) => {
        setPrimerNombre(e.target.value);
        setNameChange(true);
    }

    const handleLastNameChange = (e) => {
        setPrimerApellido(e.target.value);
        setLastNameChange(true);
    }

    const handleDocumentChange = (e) => {
        setNumeroIdentificacion(e.target.value);
        setDocumentChange(true);
    }

    const handleEditUser = async (e) => {
        e.preventDefault();
        let updatedUser = {
            "numero_identificacion": user.numero_identificacion,
            "info": {
                "primer_apellido": primerApellido,
                "segundo_apellido": segundoApellido,
                "primer_nombre": primerNombre,
                "segundo_nombre": segundoNombre,
                "pais": pais,
                "tipo_identificacion": tipoIdentificacion,
                "numero_identificacion": numeroIdentificacion,
                "email": "daniel.ramirez@cidenet.com.co",
                "fecha_ingreso": ingreso,
                "area": area,
                "estado": user.estado
            }
        }

        let existDocument = await UserHelpers.verifyDocument(updatedUser.info.numero_identificacion, documentChange);
         
        if(existDocument) {
            alert('El numero de documento ya existe');
            setOpenModal(false);
        } else {
            const email = await usersHelper.verifyEmail(updatedUser, nameChange, lastNameChange);
            updatedUser.info.email = email;
            UserHelpers.updateUser(updatedUser)
        }
    }

    const cancelEdit = () => {
        setOpenModal(false);
        setUserToEdit({})
    }

    return (
        <form className="card rounded-3xl mx-auto" onSubmit={(e) => handleEditUser(e)}>
            <div className="card-body flex flex-col justify-center">
                <p className="card-text">Email: {user.email}</p>
                <br/>
                <div className="row">
                    <div className="col">
                        <label htmlFor="name1">Primer nombre: </label>
                        <input type="text" className="form-control" name="name1" value={primerNombre} onChange={(e) => handleNameChange(e)} required/>
                    </div>
                    <div className="col">
                        <label htmlFor="name2">Segundo nombre: </label>
                        <input type="text" className="form-control" name="name2" value={segundoNombre} onChange={(e) => setSegundoNombre(e.target.value)} />
                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <label htmlFor="lastname1">Primer apellido: </label>
                        <input type="text" className="form-control" name="lastname1" value={primerApellido} onChange={(e) => handleLastNameChange(e)} required />
                    </div>
                    <div className="col">
                        <label htmlFor="lastname2">Segundo apellido: </label>
                        <input type="text" className="form-control" name="lastname2" value={segundoApellido} onChange={(e) => setSegundoApellido(e.target.value)} required />
                    </div>
                </div>
                <br/>

                <p className="card-text">Pais:
                    <select name="pais" className="form-control" aria-label="pais" defaultValue={pais} onChange={(e) => setPais(e.target.value)} required>
                        <option value="" selected disabled hidden>Selecciona</option>
                        <option value="colombia">Colombia</option>
                        <option value="Estados Unidos">Estados Unidos</option>
                    </select>
                </p>
                <br/>

                <div className="row">
                    <p>Documento de identidad:</p>
                    <div className="col">
                        <select name="documento" className="form-control" aria-label="tipo documento" defaultValue={tipoIdentificacion} onChange={(e) => setTipoIdentificacion(e.target.value)} required>
                            <option value="" selected disabled hidden>Selecciona</option>
                            <option value="cc">Cédula de Ciudadanía</option>
                            <option value="ce">Cédula de Extranjería</option>
                            <option value="pas">Pasaporte</option>
                            <option value="per">Permiso Especial</option>
                        </select>
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" name="document" defaultValue={numeroIdentificacion} onChange={(e) => handleDocumentChange(e)} required />
                    </div>
                </div>
                <br/>

                <p className="card-text">Fecha de ingreso:
                    <input type="date" className="form-control" name="ingreso" onChange={(e) => setIngreso(e.target.value)} min={minDate} max={maxDate} required />
                </p>
                <br/>

                <p className="card-text">Area:
                    <select name="area" className="form-control" aria-label="area" defaultValue={area} onChange={(e) => setArea(e.target.value)} required>
                        <option value="" selected disabled hidden>Selecciona</option>
                        <option value="Administración">Administración</option>
                        <option value="Financiera">Financiera</option>
                        <option value="Compras">Compras</option>
                        <option value="Infraestructura">Infraestructura</option>
                        <option value="Operación">Operación</option>
                        <option value="Talento Humano">Talento Humano</option>
                        <option value="Servicios Varios">Servicios Varios</option>
                    </select>
                </p>
                <br/>

                <p className="card-text">Estado: {user.estado ? 'ACTIVO' : 'INACTIVO'}</p>

                <p className="card-text">Fecha de registro: {UserHelpers.formatDate(user.updatedAt)}</p>
                
            </div>
            <div className="card-footer">
                <div className="row">
                    <div className="col flex flex-row justify-center">
                        <button
                            className="btn btn-danger"
                            onClick={() => cancelEdit()}
                        >
                            Cancelar
                        </button>
                        <button
                            type='submit'
                            className="btn btn-success ms-5"
                        >
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        </form>
    )
}
