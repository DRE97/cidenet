import React, { useState } from 'react'

// Helpers
import UserHelpers from '../../helpers/users.helper';

// Partial components
import { BiEditAlt } from 'react-icons/bi';
import { MdDelete } from 'react-icons/md';

export default function User({ user, setOpenModal, setUserToEdit }) {
    const nombre_completo = user.primer_nombre + ' ' + user.segundo_nombre + ' ' + user.primer_apellido + ' ' + user.segundo_apellido;

    const handleEditView = () => {
        setUserToEdit(user)
        setOpenModal(true);
    }

    const deleteUser = (id) => {
        if(window.confirm("Está seguro de que desea eliminar el empleado ?")) {
            UserHelpers.deleteUser(id);
        }
    }

    return (
        <tr className="h-20 px-2">
            <td className="px-3">{nombre_completo}</td>
            <td>{user.pais.replace('colombia', 'Colombia')}</td>
            <td>{UserHelpers.formatDocument(user.tipo_identificacion)} {user.numero_identificacion}</td>
            <td>{user.email}</td>
            <td>{UserHelpers.formatDate(user.fecha_ingreso)}</td>
            <td>{user.area}</td>
            <td>{user.estado ? 'ACTIVO' : 'INACTIVO'}</td>
            <td>{UserHelpers.formatDate(user.updatedAt)}</td>
            <td>
                <ul className="user-options flex flex-col justify-between">
                    <li
                        className="flex flex-row justify-evenly"
                        onClick={() => handleEditView()}
                    >
                        <BiEditAlt className="my-auto" />
                        Editar
                    </li>
                    <li
                        className="flex flex-row justify-evenly"
                        onClick={() => deleteUser(user.numero_identificacion)}
                    >
                        <MdDelete className="my-auto" />
                        Eliminar
                    </li>
                </ul>
            </td>
        </tr>
    )
}
