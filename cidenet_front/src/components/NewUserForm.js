import React, { useState, useEffect } from 'react';
import UserHelpers from '../helpers/users.helper';

export default function NewUserForm() {
    const [primerNombre, setPrimerNombre] = useState('');
    const [segundoNombre, setSegundoNombre] = useState('');
    const [primerApellido, setPrimerApellido] = useState('');
    const [segundoApellido, setSegundoApellido] = useState('');
    const [pais, setPais] = useState('');
    const [tipoIdentificacion, setTipoIdentificacion] = useState('');
    const [numeroIdentificacion, setNumeroIdentificacion] = useState('');
    const [ingreso, setIngreso] = useState('');
    const [area, setArea] = useState('');

    const [maxDate, setMaxDate] = useState('');
    const [minDate, setMinDate] = useState('');

    useEffect(() => {
        const date = UserHelpers.getDateConstrains();
        setMaxDate(date.max);
        setMinDate(date.min);
    }, [])

    const createUser = async (e) => {
        e.preventDefault();
        let userData = {
            "primer_apellido": primerApellido,
            "segundo_apellido": segundoApellido,
            "primer_nombre": primerNombre,
            "segundo_nombre": segundoNombre,
            "pais": pais,
            "tipo_identificacion": tipoIdentificacion,
            "numero_identificacion": numeroIdentificacion,
            "fecha_ingreso": ingreso,
            "area": area,
            "estado": true
        }

        const existDocument = await UserHelpers.verifyDocument(userData.numero_identificacion);
        if(!existDocument) {
            const email = await UserHelpers.generateEmail(userData)
            userData.email = email;
            UserHelpers.createUser(userData);
        } else {
            alert('El numero de documento ya existe');
        }
    }


    return (
        <div className="w-full lg:w-2/3 my-5 mx-auto">
            <div className="mx-auto mt-4 mb-4 rounded-3xl">
                    <div className="card">
                        <div className="card-body">
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item">
                                    <form onSubmit={(e) => createUser(e)}>
                                        <div className="row">
                                            <div className="col">
                                                <label htmlFor="name1">Primer nombre: </label>
                                                <input type="text" className="form-control" name="name1" onChange={(e) => setPrimerNombre(e.target.value)} maxLength="20" required />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="name2">Segundo nombre: </label>
                                                <input type="text" className="form-control" name="name2" onChange={(e) => setSegundoNombre(e.target.value)} maxLength="20" />
                                            </div>
                                        </div>
                                        <br/>
                                        <div className="row">
                                            <div className="col">
                                                <label htmlFor="lastname1">Primer apellido: </label>
                                                <input type="text" className="form-control" name="lastname1" onChange={(e) => setPrimerApellido(e.target.value)} maxLength="20" required />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="lastname2">Segundo apellido: </label>
                                                <input type="text" className="form-control" name="lastname2" onChange={(e) => setSegundoApellido(e.target.value)} maxLength="20" required />
                                            </div>
                                        </div>
                                        <br/>
                                        <div className="form-group">
                                            <label htmlFor="pais">Pais: </label>
                                            <select name="pais" className="form-control" aria-label="pais" onChange={(e) => setPais(e.target.value)} required>
                                                <option value="" selected disabled hidden>Selecciona</option>
                                                <option value="colombia">Colombia</option>
                                                <option value="Estados Unidos">Estados Unidos</option>
                                            </select>
                                        </div>
                                        <br/>

                                        <div className="row">
                                            <div className="col">
                                                <label htmlFor="documento">Tipo de documento: </label>
                                                <select name="documento" className="form-control" aria-label="tipo documento" onChange={(e) => setTipoIdentificacion(e.target.value)} required>
                                                    <option value="" selected disabled hidden>Selecciona</option>
                                                    <option value="Cédula de Ciudadanía">Cédula de Ciudadanía</option>
                                                    <option value="Cédula de Extranjería">Cédula de Extranjería</option>
                                                    <option value="Pasaporte">Pasaporte</option>
                                                    <option value="Permiso Especial">Permiso Especial</option>
                                                </select>
                                            </div>
                                            <div className="col">
                                                <label htmlFor="document">Numero de Identificacion: </label>
                                                <input type="text" className="form-control" name="document" onChange={(e) => setNumeroIdentificacion(e.target.value)} maxLength="20" required />
                                            </div>
                                        </div>
                                        <br/>
                                        <div className="form-group">
                                            <label htmlFor="ingreso">Fecha de ingreso:</label>
                                            <input type="date" className="form-control" name="ingreso" onChange={(e) => setIngreso(e.target.value)} min={minDate} max={maxDate} />
                                        </div>
                                        <br/>
                                        <div className="form-group">
                                            <label htmlFor="area">Area: </label>
                                            <select name="area" className="form-control" aria-label="area" onChange={(e) => setArea(e.target.value)} required>
                                                <option value="" selected disabled hidden>Selecciona</option>
                                                <option value="Administración">Administración</option>
                                                <option value="Financiera">Financiera</option>
                                                <option value="Compras">Compras</option>
                                                <option value="Infraestructura">Infraestructura</option>
                                                <option value="Operación">Operación</option>
                                                <option value="Talento Humano">Talento Humano</option>
                                                <option value="Servicios Varios">Servicios Varios</option>
                                            </select>
                                        </div>
                                        <br/>
                                        <br/>
                                        <div className="row">
                                            <div className="col d-flex justify-content-center">
                                                <button type="submit" className="btn btn-success">Crear Usuario</button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    )
}
