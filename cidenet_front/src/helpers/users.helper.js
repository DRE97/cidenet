import axios from 'axios';

class UserHelpers {
    constructor() {
        this.base_url = 'http://localhost:4000/api';
        this.colDomain = 'cidenet.com.co';
        this.usDomain = 'cidenet.com.us';
    }

    getAllUsers = async () => {
        const response = await axios.get(`${this.base_url}/all-users`);
        return response.data;
    }

    createUser = async (userData) => {
        await axios.post(`${this.base_url}/new-user`, userData);
        window.location.href = 'http://localhost:3000/'
    }

    updateUser = async (updatedUser) => {
        await axios.put(`${this.base_url}/update-user`, updatedUser)
        window.location.reload()
    }

    deleteUser = async (id) => {
        const data = {
            "numero_identificacion": id
        }
        await axios.delete(`${this.base_url}/delete-user`, {
            data
        })
        window.location.reload()
    }

    verifyEmail = async (data, nameChange, lastNameChange) => {
        const userData = data.info;

        if(nameChange || lastNameChange) {
            const verifyData = {
                "primer_nombre": userData.primer_nombre,
                "primer_apellido": userData.primer_apellido
            }
            
            //Validar con server primer nombre y primer apellido
            const response = await axios.post(`${this.base_url}/validate-email`, verifyData)
            const responseData = response.data;
            console.log(responseData);
            let baseEmail = '';
            let email = '';
            let primer_nombre = userData.primer_nombre;
            let primer_apellido = userData.primer_apellido;

            //Quitar mayusculas
            primer_nombre = primer_nombre.toLowerCase();
            primer_apellido = primer_apellido.toLowerCase();

            //Quitar espacios en blanco
            primer_nombre = primer_nombre.replace(/\s+/g, '');
            primer_apellido = primer_apellido.replace(/\s+/g, '');


            if(responseData.exist) {
                baseEmail = `${primer_nombre}.${primer_apellido}.${responseData.ID}`;
            } else {
                baseEmail = `${primer_nombre}.${primer_apellido}`;
            }


            if(userData.pais === 'colombia') {
                email = `${baseEmail}@${this.colDomain}`
            } else {
                email = `${baseEmail}@${this.usDomain}`
            }

            return email;
        } else {
            return userData.email
        }
    }

    generateEmail = async (userData) => {
        const verifyData = {
            "primer_nombre": userData.primer_nombre,
            "primer_apellido": userData.primer_apellido
        }

        //Validar con server primer nombre y primer apellido
        const response = await axios.post(`${this.base_url}/validate-email`, verifyData)
        const responseData = response.data;
        
        let baseEmail = '';
        let email = '';
        let primer_nombre = userData.primer_nombre;
        let primer_apellido = userData.primer_apellido;

        //Quitar mayusculas
        primer_nombre = primer_nombre.toLowerCase();
        primer_apellido = primer_apellido.toLowerCase();

        //Quitar espacios en blanco
        primer_nombre = primer_nombre.replace(/\s+/g, '');
        primer_apellido = primer_apellido.replace(/\s+/g, '');


        if(responseData.exist) {
            baseEmail = `${primer_nombre}.${primer_apellido}.${responseData.ID}`;
        } else {
            baseEmail = `${primer_nombre}.${primer_apellido}`;
        }


        if(userData.pais === 'colombia') {
            email = `${baseEmail}@${this.colDomain}`
        } else {
            email = `${baseEmail}@${this.usDomain}`
        }

        return email;
    }

    verifyDocument = async (document, documentChange) => {
        if(documentChange) {
            const response = await axios.post(`${this.base_url}/validate-document`, { numero_identificacion: document })
            const responseData = response.data;
            return responseData.exist;
        } else {
            return false
        }
    }

    getDateConstrains = () => {
        let today = new Date();
        let year = today.getUTCFullYear();
        let month = today.getMonth() + 1;
        let month_ago = month;
        let day = today.getDate();
        

        if(day < 10) {
            day = '0' + day;
        }

        if(month < 10) {
            month = '0' + month;
            month_ago = '0' + (month_ago - 1)
        }

        const max = `${year}-${month}-${day}`;
        const min = `${year}-${month_ago}-${day}`;

        return { max, min }
    }

    formatDate = (date) => {
        let format_date = date;
        format_date = format_date.split('T').shift().trim();
        format_date = format_date.split('-');

        format_date = format_date[2] + '/' + format_date[1] + '/' + format_date[0]
        
        return format_date
    }

    formatDocument = (document) => {
        let doc = document.replace('cedula', 'Cédula de Ciudadanía')
        if(doc === 'Cédula de Ciudadanía') {
            doc = 'CC'
        } else if(doc === 'Cédula de Extranjería') {
            doc = 'CE'
        } else if(doc === 'Pasaporte') {
            doc = 'PA'
        } else {
            doc = 'PE'
        }

        return doc
    }

}

export default new UserHelpers();