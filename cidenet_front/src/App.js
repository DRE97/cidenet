import React, { useState } from 'react'
import './styles/main.output.css'

// Styled components
import Navbar from './components/Navbar';
import MainContent from './components/MainContent';

function App() {

  const [content, setContent] = useState('users')

  return (
    <div className="h-screen w-screen flex flex-col lg:flex-row overflow-hidden">
      <Navbar setContent={setContent} content={content}/>
      <MainContent content={content} />
    </div>
  );
}

export default App;
