import User from '../models/User';

export const createUser = async (req, res) => {
    const userData = req.body;
    const newUser = new User(userData);
    
    if(!newUser) {
        return res.status(404).json({ message: 'No fue posible crear el usuario' })
    }

    const savedUser = await newUser.save();

    return res.status(200).json(savedUser);
}

export const getUsers = async (req, res) => {
    const usersList = await User.find();
    
    return res.status(200).json(usersList);
}

export const deleteUser = async (req, res) => {
    const { numero_identificacion } = req.body;
    await User.findOneAndDelete({ numero_identificacion })
    
    return res.status(200).json({ message: 'Usuario eliminado exitosamente' });
}

export const updateUser = async (req, res) => {
    const { numero_identificacion, info } = req.body;
    await User.findOneAndUpdate({ numero_identificacion }, info);

    return res.status(200).json({ message: 'Usuario actualizado exitosamente' });
}

export const validateEmail = async (req, res) => {
    const { primer_nombre, primer_apellido } = req.body;
    const users = await User.find({ primer_nombre, primer_apellido });

    if(users.length > 0) {
        return res.status(200).json({ ID: users.length, exist: true })
    } else {
        return res.status(200).json({ ID: users.length, exist: false });
    }
}

export const validateDocument = async (req, res) => {
    const { numero_identificacion } = req.body;
    const users = await User.find({ numero_identificacion });

    if(users.length > 0) {
        return res.status(200).json({ exist: true })
    } else {
        return res.status(200).json({ exist: false });
    }
}