import app from './app';
import './database'

// Start the server
app.listen(4000, () => {
    console.log('Server on port ', 4000);
})