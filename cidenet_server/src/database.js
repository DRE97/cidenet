import mongoose from 'mongoose';
import dbConfig from './config/db.config.json';

const uri = dbConfig.URI;

mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
    .then(() => console.log('Db is connected'))
    .catch((e) => console.log(e))
