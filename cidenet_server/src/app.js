import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

// Import routes
import usersRoutes from './routes/users.routes';

const app = express();

// Settings
app.use(morgan('dev'));
app.use(cors());
app.use(express.json());


// Routes
app.use('/api', usersRoutes);

export default app;