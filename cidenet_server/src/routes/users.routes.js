import { Router } from 'express';
import * as userControllers from '../controllers/user.controllers';

const router = Router();

// Users routes
router.post('/new-user', userControllers.createUser);
router.get('/all-users', userControllers.getUsers);
router.delete('/delete-user', userControllers.deleteUser);
router.put('/update-user', userControllers.updateUser);
router.post('/validate-email', userControllers.validateEmail);
router.post('/validate-document', userControllers.validateDocument);


export default router;