import { Schema, model } from 'mongoose';

const User = new Schema({
    "primer_apellido": { type: String, required: true },
    "segundo_apellido": { type: String, required: true },
    "primer_nombre": { type: String, required: true },
    "segundo_nombre": { type: String, required: false },
    "pais": { type: String, required: true },
    "tipo_identificacion": { type: String, required: true },
    "numero_identificacion": { type: String, required: true },
    "email": { type: String, required: true },
    "fecha_ingreso": { type: String, required: true },
    "area": { type: String, required: true },
    "estado": { type: Boolean, required: true }
}, {
    timestamps: true
});

export default model('user', User);